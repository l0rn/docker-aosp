FROM ubuntu:18.04

RUN apt-get -yq update && \
    apt-get -yqq install \
    openjdk-8-jdk ssh python git git-core gnupg bc bison flex gperf build-essential zip \
    curl zlib1g-dev gcc-multilib gcc-multilib g++-multilib libc6-dev-i386 \
    libncurses5-dev lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev \
    libgl1-mesa-dev libxml2-utils xsltproc unzip \
    libxml2-utils lzop pngcrush schedtool xsltproc \
    software-properties-common \
    gettext python python-libxml2 yasm \
    squashfs-tools genisoimage dosfstools mtools liblz4-tool rsync ccache

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://storage.googleapis.com/git-repo-downloads/repo /usr/local/bin/
RUN chmod 755 /usr/local/bin/*

RUN mkdir -p /build
WORKDIR /build

RUN repo init -u https://android.googlesource.com/platform/manifest --depth=1
RUN repo sync
